package com.example.vishwa.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.vishwa.android.R;
import com.example.vishwa.android.model.UserContact;
import com.example.vishwa.android.utils.customUI.CustomTextView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class ContactDetailActivity extends AppCompatActivity {

    private DatabaseReference mDatabase;
    UserContact userContact;
    CircleImageView userContactImage;
    CustomTextView userContactName, userContactPhone, userContactAddress;
    String contactUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDatabase = FirebaseDatabase.getInstance().getReference("userContact");

        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        setSupportActionBar(toolbar);

        getIntentData();
        initializeUI();

        retrieveContactFromServer();

    }

    private void initializeUI() {
        userContactImage = (CircleImageView) findViewById(R.id.user_contact_image);
        userContactName = (CustomTextView) findViewById(R.id.user_contact_name);
        userContactPhone = (CustomTextView) findViewById(R.id.user_contact_phone);
        userContactAddress = (CustomTextView) findViewById(R.id.user_contact_address);
    }

    private void getIntentData() {
        Intent data = getIntent();
        if (data != null) {
            contactUserId = data.getStringExtra("contactUserId");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_edit) {
            Intent intent = new Intent(ContactDetailActivity.this, EditContactActivity.class);
            intent.putExtra("contactUserId", contactUserId);
            startActivity(intent);
        }

        if (id == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    public void retrieveContactFromServer() {

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d("Snapshot", dataSnapshot.child(contactUserId).child("contactUserLastName").getValue() + "");

                userContact = dataSnapshot.child(contactUserId).getValue(UserContact.class);

                assert userContact != null;
                userContactName.setText(userContact.getContactUserFirstName() + " " + userContact.getContactUserLastName());
                userContactPhone.setText(userContact.getContactUserPhone());
                userContactAddress.setText(userContact.getContactUserAddress());
                Picasso.with(getApplicationContext())
                        .load(userContact.getContactUserImageUrl())
                        .placeholder(R.drawable.ic_user)
                        .into(userContactImage);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
