package com.example.vishwa.android.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.example.vishwa.android.R;
import com.example.vishwa.android.activity.ContactDetailActivity;
import com.example.vishwa.android.model.UserContact;
import com.example.vishwa.android.utils.customUI.CustomTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ViewHolder> {
    ArrayList<UserContact> mContactArrayList;
    Context mContext;

    public ContactListAdapter(ArrayList<UserContact> mContactArrayList, Context mContext) {
        this.mContactArrayList = mContactArrayList;
        this.mContext = mContext;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_list_adapter, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        String url = mContactArrayList.get(position).getContactUserImageUrl();
        holder.userName.setText(mContactArrayList.get(position).getContactUserFirstName() + " " + mContactArrayList.get(position).getContactUserLastName());
        holder.userPhone.setText(mContactArrayList.get(position).getContactUserPhone());
        /*Glide.with(mContext)
                .load(url)
                .into(holder.userImage);*/

        Picasso.with(mContext)
                .load(url)
                .placeholder(R.drawable.ic_user)
                .into(holder.userImage);
        holder.contactListLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext.getApplicationContext(), ContactDetailActivity.class);
                intent.putExtra("contactUserId", mContactArrayList.get(position).getContactUserId());
                mContext.startActivity(intent);
            }
        });

    }


    @Override
    public int getItemCount() {
        return mContactArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CustomTextView userName, userPhone;
        RelativeLayout contactListLayout;
        CircleImageView userImage;

        public ViewHolder(View itemView) {
            super(itemView);
            userName = (CustomTextView) itemView.findViewById(R.id.tv_user_name);
            userPhone = (CustomTextView) itemView.findViewById(R.id.tv_phone_number);
            userImage = (CircleImageView) itemView.findViewById(R.id.user_image);
            contactListLayout = (RelativeLayout) itemView.findViewById(R.id.contact_list_layout);
        }
    }
}
