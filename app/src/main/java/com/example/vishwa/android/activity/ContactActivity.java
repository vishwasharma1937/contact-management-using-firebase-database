package com.example.vishwa.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.example.vishwa.android.R;
import com.example.vishwa.android.adapter.ContactListAdapter;
import com.example.vishwa.android.model.UserContact;
import com.example.vishwa.android.utils.Utility;
import com.example.vishwa.android.utils.customUI.CustomTextView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ContactActivity extends AppCompatActivity implements View.OnClickListener {
    private DatabaseReference mDatabase;
    UserContact userContact;
    RecyclerView contactListRecycler;
    private LinearLayoutManager linearLayoutManager;
    ContactListAdapter contactListAdapter;
    ProgressBar contactProgressBar;
    CustomTextView noContact;
    CoordinatorLayout coordinateLayoutContact;
    CustomTextView tvRetry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDatabase = FirebaseDatabase.getInstance().getReference("userContact");

        initializeUI();

        if (!Utility.isInternetConnectionAvailable(this)) {
            contactProgressBar.setVisibility(View.INVISIBLE);
            Utility.noInternetDialogue(this);
            tvRetry.setVisibility(View.VISIBLE);
        } else if (!Utility.isNetworkConnectionAvailable(this)) {
            contactProgressBar.setVisibility(View.INVISIBLE);
            tvRetry.setVisibility(View.VISIBLE);
            Utility.launchSnackBar(coordinateLayoutContact, getResources().getString(R.string.no_network_message));
        } else {
            retrieveContactFromServer();
        }


    }

    private void initializeUI() {
        contactListRecycler = (RecyclerView) findViewById(R.id.contact_list_recycler);
        contactProgressBar = (ProgressBar) findViewById(R.id.contact_progress_bar);
        noContact = (CustomTextView) findViewById(R.id.no_contact);
        tvRetry = (CustomTextView) findViewById(R.id.retry);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        contactListRecycler.setLayoutManager(linearLayoutManager);

        tvRetry.setOnClickListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add) {
            addNewContact();
        }

        return super.onOptionsItemSelected(item);
    }

    private void addNewContact() {
        startActivity(new Intent(ContactActivity.this, AddNewContactActivity.class));
    }

    public void retrieveContactFromServer() {

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<UserContact> contactArrayList = new ArrayList<>();
                String key;

                Log.d("Snapshot", dataSnapshot.getValue() + "");

                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    Log.d("Key", data.getKey());
                    key = data.getKey();
                    userContact = data.getValue(UserContact.class);
                    contactArrayList.add(userContact);
                }

                if (!contactArrayList.isEmpty()) {
                    for (int i = 0; i < contactArrayList.size(); i++) {
                        Log.d("Size", contactArrayList.size() + "");
                        Log.d("Value", contactArrayList.get(i).getContactUserId());

                        contactListAdapter = new ContactListAdapter(contactArrayList, ContactActivity.this);
                        contactListRecycler.setAdapter(contactListAdapter);

                    }
                    contactProgressBar.setVisibility(View.INVISIBLE);
                    tvRetry.setVisibility(View.INVISIBLE);
                } else {
                    noContact.setVisibility(View.VISIBLE);
                    contactProgressBar.setVisibility(View.INVISIBLE);
                    tvRetry.setVisibility(View.INVISIBLE);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.retry:
                if (!Utility.isInternetConnectionAvailable(this)) {
                    contactProgressBar.setVisibility(View.INVISIBLE);
                    Utility.noInternetDialogue(this);
                    tvRetry.setVisibility(View.VISIBLE);
                } else if (!Utility.isNetworkConnectionAvailable(this)) {
                    contactProgressBar.setVisibility(View.INVISIBLE);
                    tvRetry.setVisibility(View.VISIBLE);
                    Utility.launchSnackBar(coordinateLayoutContact, getResources().getString(R.string.no_network_message));
                } else {
                    retrieveContactFromServer();
                }
                break;
        }
    }
}
