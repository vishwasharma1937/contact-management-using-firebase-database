package com.example.vishwa.android.utils.customUI;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import com.example.vishwa.android.R;

public class CustomEditText extends AppCompatEditText {

    public CustomEditText(Context context) {
        super(context);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);

    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setCustomFont(context, attrs);
    }

    private void setCustomFont(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomTextView);
        String customFont = typedArray.getString(R.styleable.CustomTextView_customFont);
        setCustomFont(context, customFont);
    }

    private void setCustomFont(Context context, String asset) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), asset);
        setTypeface(typeface);
    }
}
