package com.example.vishwa.android.model;


public class UserContact {
    String contactUserId, contactUserFirstName, contactUserLastName, contactUserAddress, contactUserImageUrl, contactUserPhone;

    public String getContactUserId() {
        return contactUserId;
    }

    public void setContactUserId(String contactUserId) {
        this.contactUserId = contactUserId;
    }

    public String getContactUserFirstName() {
        return contactUserFirstName;
    }

    public void setContactUserFirstName(String contactUserFirstName) {
        this.contactUserFirstName = contactUserFirstName;
    }

    public String getContactUserLastName() {
        return contactUserLastName;
    }

    public void setContactUserLastName(String contactUserLastName) {
        this.contactUserLastName = contactUserLastName;
    }

    public String getContactUserAddress() {
        return contactUserAddress;
    }

    public void setContactUserAddress(String contactUserAddress) {
        this.contactUserAddress = contactUserAddress;
    }

    public String getContactUserImageUrl() {
        return contactUserImageUrl;
    }

    public void setContactUserImageUrl(String contactUserImageUrl) {
        this.contactUserImageUrl = contactUserImageUrl;
    }

    public String getContactUserPhone() {
        return contactUserPhone;
    }

    public void setContactUserPhone(String contactUserPhone) {
        this.contactUserPhone = contactUserPhone;
    }
}
