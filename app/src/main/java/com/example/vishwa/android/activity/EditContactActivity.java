package com.example.vishwa.android.activity;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.vishwa.android.R;
import com.example.vishwa.android.model.UserContact;
import com.example.vishwa.android.utils.Constants;
import com.example.vishwa.android.utils.Utility;
import com.example.vishwa.android.utils.customUI.CustomEditText;
import com.example.vishwa.android.utils.customUI.CustomTextView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.example.vishwa.android.utils.Constants.REQUEST_GALLERY_IMAGE;

public class EditContactActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {

    private DatabaseReference mDatabase, mDataStorageRef;
    CircleImageView userConatctImage;
    CustomEditText userFirstName, userLastName, userPhone, userAddress;
    String contactUserId;
    UserContact userContact;
    CustomTextView tvCancel, tvUpdate;
    String uFName, uLName, uAddress, uPhone;
    private Uri selectedImage;
    private String mediaPath;
    ProgressBar editContactProgressBar;
    private StorageReference storageReference;
    String contactId;
    RelativeLayout editContactLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_contact);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getIntentData();

        mDatabase = FirebaseDatabase.getInstance().getReference("userContact");

        storageReference = FirebaseStorage.getInstance().getReference();

        mDataStorageRef = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_UPLOADS);

        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        setSupportActionBar(toolbar);

        initializeUI();

        retrieveContactFromServer();
    }

    private void getIntentData() {
        Intent data = getIntent();
        if (data != null) {
            contactUserId = data.getStringExtra("contactUserId");
        }
    }


    private void initializeUI() {
        userConatctImage = (CircleImageView) findViewById(R.id.user_edit_image);
        userFirstName = (CustomEditText) findViewById(R.id.user_edit_first_name);
        userLastName = (CustomEditText) findViewById(R.id.user_edit_last_name);
        userPhone = (CustomEditText) findViewById(R.id.user_edit_phone_number);
        userAddress = (CustomEditText) findViewById(R.id.user_edit_address);
        editContactProgressBar = (ProgressBar) findViewById(R.id.edit_contact_progress_bar);
        editContactLayout = (RelativeLayout) findViewById(R.id.edit_contact_layout);

        tvCancel = (CustomTextView) findViewById(R.id.edit_tv_cancel);
        tvUpdate = (CustomTextView) findViewById(R.id.edit_tv_update);

        tvCancel.setOnClickListener(this);
        tvUpdate.setOnClickListener(this);
        userConatctImage.setOnClickListener(this);

        editContactLayout.setOnTouchListener(this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    public void retrieveContactFromServer() {

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                userContact = dataSnapshot.child(contactUserId).getValue(UserContact.class);

                if (userContact != null) {
                    userFirstName.setText(userContact.getContactUserFirstName());
                    userLastName.setText(userContact.getContactUserLastName());
                    userPhone.setText(userContact.getContactUserPhone());
                    userAddress.setText(userContact.getContactUserAddress());
                    Picasso.with(getApplicationContext())
                            .load(userContact.getContactUserImageUrl())
                            .placeholder(R.drawable.ic_user)
                            .into(userConatctImage);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edit_tv_cancel:
                onBackPressed();
                break;
            case R.id.edit_tv_update:
                if (!validateUser()) {
                    return;
                } else if (selectedImage != null) {
                    updateContact();
                } else {
                    addContact();
                    startConatctActivity();
                    Utility.hideSoftKeyboard(this, v);
                }
                break;

            case R.id.user_edit_image:
                pickImageFromGallery();
                break;
        }
    }

    private boolean validateUser() {

        getUserValue();

        if (uFName.isEmpty()) {
            userFirstName.setError(getResources().getString(R.string.user_first_name_empty_warn));
            requestFocus(userFirstName);
            return false;
        } else if (uLName.isEmpty()) {
            userLastName.setError(getResources().getString(R.string.user_last_name_empty_warn));
            requestFocus(userLastName);
            return false;
        } else if (uPhone.isEmpty()) {
            userPhone.setError(getResources().getString(R.string.user_phone_empty_warn));
            requestFocus(userPhone);
            return false;
        } else if (uAddress.isEmpty()) {
            userAddress.setError(getResources().getString(R.string.user_address_empty_warn));
            requestFocus(userAddress);
            return false;
        }
        return true;
    }

    private void getUserValue() {
        uFName = userFirstName.getText().toString().trim();
        uLName = userLastName.getText().toString().trim();
        uPhone = userPhone.getText().toString().trim();
        uAddress = userAddress.getText().toString().trim();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

    }

    private void addContact() {

        UserContact userCon = new UserContact();

        mDatabase = FirebaseDatabase.getInstance().getReference();

        userCon.setContactUserId(contactUserId);
        userCon.setContactUserFirstName(uFName);
        userCon.setContactUserLastName(uLName);
        userCon.setContactUserPhone(uPhone);
        userCon.setContactUserAddress(uAddress);
        userCon.setContactUserImageUrl(userContact.getContactUserImageUrl());

        mDatabase.child("userContact").child(contactUserId).setValue(userCon);

        Log.d("Url", userContact.getContactUserImageUrl());
    }

    private void pickImageFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(Intent.createChooser(intent, "Select File"), REQUEST_GALLERY_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_GALLERY_IMAGE) {
            if (resultCode == RESULT_OK) {
                selectImageFromGallery(data);

            } else {
                userConatctImage.setImageResource(R.drawable.ic_user);
            }
        }

    }

    private void selectImageFromGallery(Intent data) {
        selectedImage = data.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
        assert cursor != null;
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        mediaPath = cursor.getString(columnIndex);


        userConatctImage.setImageBitmap(BitmapFactory.decodeFile(mediaPath));

        cursor.close();
    }

    public String getFileExtension(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }


    private void updateContact() {

        editContactProgressBar.setVisibility(View.VISIBLE);

        StorageReference storageRef = storageReference.child(Constants.STORAGE_PATH_UPLOADS + System.currentTimeMillis() + "." + getFileExtension(selectedImage));

        storageRef.putFile(selectedImage)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        UserContact userContactModel = new UserContact();

                        mDatabase = FirebaseDatabase.getInstance().getReference();

                        userContactModel.setContactUserId(contactUserId);
                        userContactModel.setContactUserFirstName(uFName);
                        userContactModel.setContactUserLastName(uLName);
                        userContactModel.setContactUserPhone(uPhone);
                        userContactModel.setContactUserAddress(uAddress);
                        userContactModel.setContactUserImageUrl(String.valueOf(taskSnapshot.getDownloadUrl()));

                        mDatabase.child("userContact").child(contactUserId).setValue(userContactModel);

                        editContactProgressBar.setVisibility(View.INVISIBLE);
                        startConatctActivity();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {

                        Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void startConatctActivity() {
        startActivity(new Intent(EditContactActivity.this, ContactActivity.class));
        finish();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Utility.hideSoftKeyboard(this, v);
        return false;
    }
}
