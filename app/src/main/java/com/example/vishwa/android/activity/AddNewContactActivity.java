package com.example.vishwa.android.activity;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.example.vishwa.android.R;
import com.example.vishwa.android.model.UserContact;
import com.example.vishwa.android.utils.Constants;
import com.example.vishwa.android.utils.Utility;
import com.example.vishwa.android.utils.customUI.CustomEditText;
import com.example.vishwa.android.utils.customUI.CustomTextView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.example.vishwa.android.utils.Constants.REQUEST_GALLERY_IMAGE;

public class AddNewContactActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {

    RelativeLayout addNewContactLayout;
    CustomEditText userFName, userLName, userAddress, userPhoneNumber;
    CustomTextView tvCancel, tvSave;
    CircleImageView userContactImage;
    String uFName, uLName, uAddress, uPhone;
    private DatabaseReference mDatabase;
    String contactId;
    private Uri selectedImage;
    private String mediaPath;
    private StorageReference storageReference;
    UserContact userContact;
    CoordinatorLayout coordinatorLayout;
    ProgressBar addContactProgressBar;
    FirebaseStorage mStorage;
    ScrollView scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_contact);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        setSupportActionBar(toolbar);

        addNewContactLayout = (RelativeLayout) findViewById(R.id.add_new_contact_layout);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinate_layout);
        scrollView = (ScrollView) findViewById(R.id.scroll_add_contact);

        storageReference = FirebaseStorage.getInstance().getReference();
        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_UPLOADS);


        Utility.hideSoftKeyboard(this, addNewContactLayout);

        initializeUI();
        checkUserPermission();
    }

    private void initializeUI() {

        userFName = (CustomEditText) findViewById(R.id.user_first_name);
        userLName = (CustomEditText) findViewById(R.id.user_last_name);
        userAddress = (CustomEditText) findViewById(R.id.user_address);
        userPhoneNumber = (CustomEditText) findViewById(R.id.user_phone_number);
        tvCancel = (CustomTextView) findViewById(R.id.tv_cancel);
        tvSave = (CustomTextView) findViewById(R.id.tv_save);
        userContactImage = (CircleImageView) findViewById(R.id.user_contact_image);
        addContactProgressBar = (ProgressBar) findViewById(R.id.add_contact_progress_bar);


        userFName.setOnClickListener(this);
        userLName.setOnClickListener(this);
        userAddress.setOnClickListener(this);
        userPhoneNumber.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        tvSave.setOnClickListener(this);
        userContactImage.setOnClickListener(this);
        addNewContactLayout.setOnTouchListener(this);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.user_contact_image:
                pickImageFromGallery();
                break;

            case R.id.tv_cancel:
                onBackPressed();
                break;

            case R.id.tv_save:
                if (!validateUser()) {
                    return;
                } else {
                    addNewContact();
                    Utility.hideSoftKeyboard(this,view);
                }
                break;
        }
    }

    private boolean validateUser() {

        getUserValue();

        if (uFName.isEmpty()) {
            userFName.setError(getResources().getString(R.string.user_first_name_empty_warn));
            requestFocus(userFName);
            return false;
        } else if (uLName.isEmpty()) {
            userLName.setError(getResources().getString(R.string.user_last_name_empty_warn));
            requestFocus(userLName);
            return false;
        } else if (uPhone.isEmpty()) {
            userPhoneNumber.setError(getResources().getString(R.string.user_phone_empty_warn));
            requestFocus(userLName);
            return false;
        } else if (uAddress.isEmpty()) {
            userAddress.setError(getResources().getString(R.string.user_address_empty_warn));
            requestFocus(userAddress);
            return false;
        }
        return true;
    }

    private void getUserValue() {
        uFName = userFName.getText().toString().trim();
        uLName = userLName.getText().toString().trim();
        uPhone = userPhoneNumber.getText().toString().trim();
        uAddress = userAddress.getText().toString().trim();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

    }

    private void checkUserPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            Dexter.withActivity(this)
                    .withPermissions(
                            Manifest.permission.READ_EXTERNAL_STORAGE
                    ).withListener(new MultiplePermissionsListener() {
                @Override
                public void onPermissionsChecked(MultiplePermissionsReport report) {
                }

                @Override
                public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                }
            }).check();
        }

    }

    private void pickImageFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(Intent.createChooser(intent, "Select File"), REQUEST_GALLERY_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_GALLERY_IMAGE) {
            if (resultCode == RESULT_OK) {
                selectImageFromGallery(data);

            } else {
                userContactImage.setImageResource(R.drawable.ic_user);
            }
        }

    }

    private void selectImageFromGallery(Intent data) {
        selectedImage = data.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
        assert cursor != null;
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        mediaPath = cursor.getString(columnIndex);


        userContactImage.setImageBitmap(BitmapFactory.decodeFile(mediaPath));

        cursor.close();
    }

    public String getFileExtension(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }


    private void addNewContact() {

        if (selectedImage != null) {

            addContactProgressBar.setVisibility(View.VISIBLE);

            StorageReference storageRef = storageReference.child(Constants.STORAGE_PATH_UPLOADS + System.currentTimeMillis() + "." + getFileExtension(selectedImage));

            storageRef.putFile(selectedImage)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            userContact = new UserContact();

                            mDatabase = FirebaseDatabase.getInstance().getReference();
                            contactId = mDatabase.push().getKey();

                            userContact.setContactUserId(contactId);
                            userContact.setContactUserFirstName(uFName);
                            userContact.setContactUserLastName(uLName);
                            userContact.setContactUserPhone(uPhone);
                            userContact.setContactUserAddress(uAddress);
                            userContact.setContactUserImageUrl(String.valueOf(taskSnapshot.getDownloadUrl()));
                            mDatabase.child("userContact").child(contactId).setValue(userContact);

                            addContactProgressBar.setVisibility(View.INVISIBLE);
                            startConatctActivity();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {

                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });

        } else {
            //display an error if no file is selected
        }
    }

    private void startConatctActivity() {
        startActivity(new Intent(AddNewContactActivity.this, ContactActivity.class));
        finish();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Utility.hideSoftKeyboard(this, v);
        return false;
    }
}
