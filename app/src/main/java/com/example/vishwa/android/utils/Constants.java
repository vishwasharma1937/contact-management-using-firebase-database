package com.example.vishwa.android.utils;


public class Constants {
    public static final int REQUEST_GALLERY_IMAGE = 1;
    public static final String STORAGE_PATH_UPLOADS = "uploads/";
    public static final String DATABASE_PATH_UPLOADS = "uploads";
}
